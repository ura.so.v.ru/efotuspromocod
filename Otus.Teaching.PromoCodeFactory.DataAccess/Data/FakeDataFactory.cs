﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static void Initialize(MyDbContext context)
        {
            if (!context.Employees.Any())
            {
                context.Employees.AddRange
                (
                   new Employee()
                   {
                       Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                       Email = "owner@somemail.ru",
                       FirstName = "Иван",
                       LastName = "Сергеев",
                       RoleId = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                       Role = context.Roles.FirstOrDefault(x => x.Name == "Admin"),
                       AppliedPromocodesCount = 5
                   },
                   new Employee()
                   {
                       Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                       Email = "andreev@somemail.ru",
                       FirstName = "Петр",
                       LastName = "Андреев",
                       RoleId = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                       Role = context.Roles.FirstOrDefault(x => x.Name == "PartnerManager"),
                       AppliedPromocodesCount = 10
                   }
                );
            }

            if (!context.Roles.Any())
            {
                context.Roles.AddRange
                (
                    new Role()
                    {
                        Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                        Name = "Admin",
                        Description = "Администратор"
                    },
                    new Role()
                    {
                        Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                        Name = "PartnerManager",
                        Description = "Партнерский менеджер"
                    }
                );
            }

            if (!context.Preferences.Any())
            {
                context.Preferences.AddRange
                (
                    new Preference()
                    {
                        Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                        Name = "Театр",
                        CustomerId = Guid.Parse("98dfaaca-784a-41ae-acff-9982f40e5840")
                    },
                    new Preference()
                    {
                        Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                        Name = "Семья",
                        CustomerId = Guid.Parse("f8993365-4ace-40d2-81cf-5419a552f10f")

                    },
                    new Preference()
                    {
                        Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                        Name = "Дети",
                        CustomerId = Guid.Parse("dd445af8-fc76-4da3-a7b0-02212ca3094f")
                    }
                );
                
            }

            if (!context.Customers.Any())
            {
                context.Customers.AddRange(
                   new Customer()
                   {
                       Id = Guid.Parse("dd445af8-fc76-4da3-a7b0-02212ca3094f"),
                       EmailAdress = "ivan_sergeev@mail.ru",
                       FirstName = "Иван",
                       LastName = "Петров",
                       //TODO: Добавить предзаполненный список предпочтений
                       Preferences = context.Preferences.Where(x => x.Name == "Дети").ToList()
                   },
                   new Customer()
                   {
                       Id = Guid.Parse("f8993365-4ace-40d2-81cf-5419a552f10f"),
                       EmailAdress = "ivan_sergeev@mail.ru",
                       FirstName = "Павел",
                       LastName = "Владимирович",
                       //TODO: Добавить предзаполненный список предпочтений
                       Preferences = context.Preferences.Where(x => x.Name == "Семья").ToList()
                   },
                   new Customer()
                   {
                       Id = Guid.Parse("98dfaaca-784a-41ae-acff-9982f40e5840"),
                       EmailAdress = "Artem_Vladimirovich@mail.ru",
                       FirstName = "Артём",
                       LastName = "Владимирович",
                       //TODO: Добавить предзаполненный список предпочтений
                       Preferences = context.Preferences.Where(x => x.Name == "Театр").ToList()
                   }
                 );
            }
            if (!context.CustomerPreferences.Any())
            {
                context.CustomerPreferences.AddRange(
                    new CustomerPreference()
                    {
                        Id = Guid.NewGuid(),
                        CustomerId = Guid.Parse("dd445af8-fc76-4da3-a7b0-02212ca3094f"),
                        PreferenceId = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84")
                    },
                    new CustomerPreference()
                    {
                        Id = Guid.NewGuid(),
                        CustomerId = Guid.Parse("f8993365-4ace-40d2-81cf-5419a552f10f"),
                        PreferenceId = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd")
                    },
                    new CustomerPreference()
                    {
                        Id = Guid.NewGuid(),
                        CustomerId = Guid.Parse("98dfaaca-784a-41ae-acff-9982f40e5840"),
                        PreferenceId = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c")
                    }
                    );
            }
            if (!context.PromoCodes.Any())
            {
                context.PromoCodes.AddRange(
                        new PromoCode()
                        {
                            Id = Guid.NewGuid(),
                            Code = "M0ZY397D",
                            ServiceInfo = "OtusPromocod",
                            BeginDate = DateTime.Now.Date,
                            EndDate = DateTime.Now.Date.AddMonths(6),
                            PartnerName = "Петр Андреев",
                            PartnerManagerId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                            PreferenceID = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                            CustomerId = Guid.Parse("dd445af8-fc76-4da3-a7b0-02212ca3094f")
                        },
                        new PromoCode()
                        {
                            Id = Guid.NewGuid(),
                            Code = "CU73N2XD",
                            ServiceInfo = "OtusPromocod",
                            BeginDate = DateTime.Now.Date.AddDays(15),
                            EndDate = DateTime.Now.Date.AddDays(15).AddMonths(6),
                            PartnerName = "Петр Андреев",
                            PartnerManagerId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                            PreferenceID = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                            CustomerId = Guid.Parse("f8993365-4ace-40d2-81cf-5419a552f10f")
                        },
                        new PromoCode()
                        {
                            Id = Guid.NewGuid(),
                            Code = "AXH72BHX",
                            ServiceInfo = "OtusPromocod",
                            BeginDate = DateTime.Now.Date.AddDays(30),
                            EndDate = DateTime.Now.Date.AddDays(30).AddMonths(6),
                            PartnerName = "Петр Андреев",
                            PartnerManagerId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                            PreferenceID = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                            CustomerId = Guid.Parse("98dfaaca-784a-41ae-acff-9982f40e5840")
                        }

                    );
            }

            context.SaveChanges();
        }
        public static IEnumerable<Employee> Employees => new List<Employee>()
        {
            new Employee()
            {
                Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                Role = Roles.FirstOrDefault(x => x.Name == "Admin"),
                AppliedPromocodesCount = 5
            },
            new Employee()
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                Role = Roles.FirstOrDefault(x => x.Name == "PartnerManager"),
                AppliedPromocodesCount = 10
            },
        };

        public static IEnumerable<Role> Roles => new List<Role>()
        {
            new Role()
            {
                Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                Name = "Admin",
                Description = "Администратор",
            },
            new Role()
            {
                Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                Name = "PartnerManager",
                Description = "Партнерский менеджер"
            }
        };
        
        public static IEnumerable<Preference> Preferences => new List<Preference>()
        {
            new Preference()
            {
                Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                Name = "Театр",
            },
            new Preference()
            {
                Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                Name = "Семья",
            },
            new Preference()
            {
                Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                Name = "Дети",
            }
        };

        public static IEnumerable<Customer> Customers
        {
            get
            {
                var customerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0");
                var customers = new List<Customer>()
                {
                    new Customer()
                    {
                        Id = customerId,
                        EmailAdress = "ivan_sergeev@mail.ru",
                        FirstName = "Иван",
                        LastName = "Петров",
                        //TODO: Добавить предзаполненный список предпочтений
                        Preferences = Preferences.Where(x => x.Name == "Дети").ToList()
                    }
                };

                return customers;
            }
        }
    }
}