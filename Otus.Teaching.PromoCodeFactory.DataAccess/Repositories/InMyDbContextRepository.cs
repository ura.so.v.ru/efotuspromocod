﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMyDbContextRepository<T> : EFRepository<T> where T : BaseEntity
    {
        private MyDbContext db;

        public InMyDbContextRepository(MyDbContext db)
        {
            this.db = db;
        }

        public async Task CreateEntity(T entity)
        {
            await db.Set<T>().AddAsync(entity);
            await db.SaveChangesAsync();
        }

        public async Task DeleteEntity(Guid id)
        {
            var entity = await db.Set<T>().FindAsync(id);
            db.Set<T>().Remove(entity);
            await db.SaveChangesAsync();
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await db.Set<T>().ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await db.Set<T>().FindAsync(id);
        }

        public async Task<IEnumerable<Preference>> SelectionEntity(Guid id)
        {
            return await db.Preferences.Where(x => x.CustomerId == id).ToListAsync();
        }
        public async Task<IEnumerable<CustomerPreference>> SelectionCustPrefer(Guid id)
        {
            return await db.CustomerPreferences.Where(x => x.CustomerId == id).ToListAsync();
        }

        public async Task UpdateEntity(T entity)
        {
            db.Set<T>().Update(entity);
            await db.SaveChangesAsync();
        }

        public async Task<Employee> GetByNameAsync(string[] fullname)
        {
            return await db.Employees.FirstOrDefaultAsync(x => x.FirstName == fullname[0] &&  x.LastName == fullname[1]);
        }

        public async Task<Preference> GetPreferenceByNameAsync(string name)
        {
            return await db.Preferences.FirstOrDefaultAsync(x => x.Name == name);
        }

        public async Task<PromoCode> GetPromoCodeByCustAsync(Guid customerId)
        {
            return await db.Set<PromoCode>().FirstOrDefaultAsync(x => x.CustomerId == customerId);
        }
    }
}
