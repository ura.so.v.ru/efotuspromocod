﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface EFRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);
        Task CreateEntity(T entity);
        Task UpdateEntity(T entity);
        Task DeleteEntity(Guid id);
        Task<IEnumerable<Preference>> SelectionEntity(Guid id);
        Task<IEnumerable<CustomerPreference>> SelectionCustPrefer(Guid id);
        Task<Employee> GetByNameAsync(string[] fullname);
        Task<Preference> GetPreferenceByNameAsync(string name);
        Task<PromoCode> GetPromoCodeByCustAsync(Guid customerId);
    }
}