﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Role
        : BaseEntity
    {
        [MaxLength(30, ErrorMessage = "Длина этого поля должна быть не больше 30 символов")]
        public string Name { get; set; }

        [MaxLength(50, ErrorMessage = "Длина этого поля должна быть не больше 30 символов")]
        public string Description { get; set; }
    }
}