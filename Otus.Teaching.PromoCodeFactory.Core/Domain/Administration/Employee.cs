﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Employee
        : BaseEntity
    {
        [MaxLength(30, ErrorMessage = "Длина этого поля должна быть не больше 30 символов")]
        public string FirstName { get; set; }

        [MaxLength(30, ErrorMessage = "Длина этого поля должна быть не больше 30 символов")]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        
        public Guid RoleId { get; set; }
        
        [ForeignKey("RoleId")]
        public Role Role { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}