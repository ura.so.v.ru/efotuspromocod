﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        [MaxLength(30, ErrorMessage = "Длина этого поля должна быть не больше 30 символов")]
        public string FirstName { get; set; }

        [MaxLength(30, ErrorMessage = "Длина этого поля должна быть не больше 30 символов")]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [DataType(DataType.EmailAddress)]
        public string EmailAdress { get; set; }

        //TODO: Списки Preferences и Promocodes 

        public List<Preference> Preferences { get; set; }
        public List<PromoCode> PromoCodes { get; set; }
    }
}