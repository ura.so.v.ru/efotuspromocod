﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        :BaseEntity
    {
        [MaxLength(30, ErrorMessage = "Длина этого поля должна быть не больше 30 символов")]
        public string Name { get; set; }
        
        [ForeignKey("Customer")]
        public Guid CustomerId { get; set; }
    }
}