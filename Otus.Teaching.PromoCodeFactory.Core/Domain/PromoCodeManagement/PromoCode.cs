﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class PromoCode
        : BaseEntity
    {
        [MaxLength(30, ErrorMessage = "Длина этого поля должна быть не больше 30 символов")]
        public string Code { get; set; }

        [MaxLength(30, ErrorMessage = "Длина этого поля должна быть не больше 30 символов")]
        public string ServiceInfo { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        [MaxLength(30, ErrorMessage = "Длина этого поля должна быть не больше 30 символов")]
        public string PartnerName { get; set; }

        public Guid PartnerManagerId { get; set; }
        public Guid PreferenceID { get; set; }
        public Guid CustomerId { get; set; }
        
        [ForeignKey("PartnerManagerId")]
        public Employee PartnerManager { get; set; }

        [ForeignKey("PreferenceID")]
        public Preference Preference { get; set; }
    }
}