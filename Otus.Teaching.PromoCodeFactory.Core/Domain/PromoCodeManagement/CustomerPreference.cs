﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class CustomerPreference : BaseEntity
    {
        public CustomerPreference()
        {

        }
        public CustomerPreference(Guid custId, Guid prefId)
        {
            CustomerId = custId;
            PreferenceId = prefId;
        }
        public Guid CustomerId { get; set; }
        public Guid PreferenceId { get; set; }
    }
}
