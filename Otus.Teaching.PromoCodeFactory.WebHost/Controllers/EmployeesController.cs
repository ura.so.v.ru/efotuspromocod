﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly EFRepository<Employee> _employeeRepository;
        private readonly EFRepository<Role> _roleRepository;
        private readonly IMapper _mapper;

        public EmployeesController(EFRepository<Employee> employeeRepository, EFRepository<Role> roleRepository, IMapper mapper)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
            _mapper = mapper;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var response = _mapper.Map<List<EmployeeShortResponse>>(employees);
            return await Task.FromResult(response);
        }
        
        /// <summary>
        /// Получить данные сотрудника по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);
            var role = await _roleRepository.GetByIdAsync(employee.RoleId);
            employee.Role = role;

            if (employee == null)
                return NotFound();

            var response = _mapper.Map<EmployeeResponse>(employee);
            return await Task.FromResult(response);
        }

        /// <summary>
        /// Добавить нового сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<string> CreateEmployee(CreateOrEditEmployee employee)
        {
            var dto = _mapper.Map<Employee>(employee);
            var role = await _roleRepository.GetByIdAsync(employee.RoleId);
            dto.Role = role;
            await _employeeRepository.CreateEntity(dto);
            return await Task.FromResult("Добавлен");
        }


        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<string> DeleteEmployee(Guid id)
        {
            await Task.Run(async () => await _employeeRepository.DeleteEntity(id));
            return await Task.FromResult("Удалён");
        }

        /// <summary>
        /// Изменить данные сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<string> EditEmployee(Guid id, CreateOrEditEmployee edit)
        {
            var dto = _mapper.Map<Employee>(edit);
            var role = await _roleRepository.GetByIdAsync(edit.RoleId);
            dto.Role = role;
            dto.Id = id;
            await _employeeRepository.UpdateEntity(dto);
            return await Task.FromResult("Данные обновлены");
        }
    }
}