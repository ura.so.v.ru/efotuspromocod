﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferenceController : Controller
    {
        private readonly EFRepository<Preference> _preferenceRepository;
        private readonly IMapper _mapper;

        public PreferenceController(EFRepository<Preference> preferenceRepository, IMapper mapper)
        {
            _preferenceRepository = preferenceRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить список всех предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<PreferenceResponse>> GetAllPreferenceAsync()
        {
            var preferences = await Task.Run(async () => await _preferenceRepository.GetAllAsync());
            var response = _mapper.Map<IEnumerable<PreferenceResponse>>(preferences);
            return await Task.FromResult(response);
        }
    }
}
