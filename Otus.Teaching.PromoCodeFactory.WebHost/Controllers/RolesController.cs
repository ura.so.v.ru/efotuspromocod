﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Роли сотрудников
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class RolesController
    {
        private readonly EFRepository<Role> _rolesRepository;
        private readonly IMapper _mapper;

        public RolesController(EFRepository<Role> rolesRepository, IMapper mapper)
        {
            _rolesRepository = rolesRepository;
            _mapper = mapper;
        }
        
        /// <summary>
        /// Получить все доступные роли сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<RoleItemResponse>> GetRolesAsync()
        {
            var roles = await _rolesRepository.GetAllAsync();

            var response = _mapper.Map<IEnumerable<RoleItemResponse>>(roles);

            return await Task.FromResult(response);
        }
        [HttpPost]
        public async Task<string> CreateRole(CreateOrEditRole role)
        {
            var dto = _mapper.Map<Role>(role);
            await _rolesRepository.CreateEntity(dto);
            return await Task.FromResult("Роль добавлена");
        }
    }
}