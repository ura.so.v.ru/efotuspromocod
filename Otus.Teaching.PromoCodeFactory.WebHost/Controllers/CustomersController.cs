﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly EFRepository<Customer> _customerRepository;
        private readonly EFRepository<Preference> _preferenceRepository;
        private readonly EFRepository<CustomerPreference> _custPrefRepository;
        private readonly EFRepository<PromoCode> _promoCodeRepository;
        private readonly IMapper _mapper;

        public CustomersController(EFRepository<Customer> customerRepository,
            EFRepository<CustomerPreference> custPrefRepository,
            EFRepository<Preference> preferenceRepository,
            EFRepository<PromoCode> promoCodeRepository,
            IMapper mapper)
        {
            _customerRepository = customerRepository;
            _mapper = mapper;
            _preferenceRepository = preferenceRepository;
            _custPrefRepository = custPrefRepository;
            _promoCodeRepository = promoCodeRepository;
        }

        /// <summary>
        /// Получить список всех клиенто
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<CustomerShortResponse>> GetCustomersAsync()
        {
            //TODO: Добавить получение списка клиентов
            var customers = await Task.Run(async () => await _customerRepository.GetAllAsync());
            var response = _mapper.Map<IEnumerable<CustomerShortResponse>>(customers);
            return await Task.FromResult(response);
        }

        /// <summary>
        /// Получить данные клиента по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            //TODO: Добавить получение клиента вместе с выданными ему промомкодами
            var customer = await Task.Run(async () => await _customerRepository.GetByIdAsync(id));


            var preferanceByCustomer = await Task.Run(async () => await _custPrefRepository.SelectionCustPrefer(id));
            List<Preference> preferences = new List<Preference>();
            foreach (var item in preferanceByCustomer)
            {
                preferences.Add(await Task.Run(async () => await _preferenceRepository.GetByIdAsync(item.PreferenceId)));
            }
            var promocod = await _promoCodeRepository.GetPromoCodeByCustAsync(id);
            var preferencesResponse = _mapper.Map<IEnumerable<PreferenceResponse>>(preferences);

            var response = _mapper.Map<CustomerResponse>(customer);
            response.Preferences = preferencesResponse.ToList();
            
            return await Task.FromResult(response);
        }

        /// <summary>
        /// Добавить нового клиента
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<string> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            //TODO: Добавить создание нового клиента вместе с его предпочтениями
            var customer = _mapper.Map<Customer>(request);
            await Task.Run(async () => await _customerRepository.CreateEntity(customer));

            List<Preference> preferences = new List<Preference>();

            foreach (var item in request.PreferenceIds)
            {
                preferences.Add(await _preferenceRepository.GetByIdAsync(item));
            }

            foreach (var item in preferences)
            {
                var custPref = new CustomerPreference(customer.Id, item.Id);
                await Task.Run(async () => await _custPrefRepository.CreateEntity(custPref));
            }
            return await Task.FromResult("Клиент добавлен");
        }

        /// <summary>
        /// Обновить данные клиента
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<string> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            //TODO: Обновить данные клиента вместе с его предпочтениями
            var customer = _mapper.Map<Customer>(request);
            customer.Id = id;
            await Task.Run(async () => await _customerRepository.UpdateEntity(customer));

            if(request.PreferenceIds.Count > 0)
            {
                List<Preference> preferences = new List<Preference>();

                foreach (var item in request.PreferenceIds)
                {
                    preferences.Add(await _preferenceRepository.GetByIdAsync(item));
                }

                var endCustPrefers = await Task.Run(async () => await _custPrefRepository.SelectionCustPrefer(id));
                foreach (var item in endCustPrefers)
                {
                    await Task.Run(async () => await _custPrefRepository.DeleteEntity(item.Id));
                }

                foreach (var item in preferences)
                {
                    var custPref = new CustomerPreference(id, item.Id);
                    await Task.Run(async () => await _custPrefRepository.CreateEntity(custPref));
                }
            }
            
            return await Task.FromResult("Клиент и его предпочтения обновлены");

        }

        /// <summary>
        /// Удалить данные клиента
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<string> DeleteCustomer(Guid id)
        {
            //TODO: Удаление клиента вместе с выданными ему промокодами
            var customer = await Task.Run(async () => await _customerRepository.GetByIdAsync(id));

            var custPrefers = await Task.Run(async () => await _custPrefRepository.SelectionCustPrefer(id));

            var preferences = await Task.Run(async () => await _preferenceRepository.SelectionEntity(id));

            var promocod = await Task.Run(async () => await _promoCodeRepository.GetPromoCodeByCustAsync(id));

            foreach (var item in custPrefers)
            {
                await Task.Run(async () => await _custPrefRepository.DeleteEntity(item.Id));
            }
            
            if (customer.PromoCodes.Count > 0 || customer.PromoCodes != null)
                await Task.Run(async () => await _promoCodeRepository.DeleteEntity(promocod.Id));

            await Task.Run(async () => await _customerRepository.DeleteEntity(id));


            return await Task.FromResult("Клиент и его промокоды удалены");
        }
    }
}