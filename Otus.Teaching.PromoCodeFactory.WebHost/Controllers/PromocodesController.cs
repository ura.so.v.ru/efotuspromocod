﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly EFRepository<Employee> _employeeRepository;
        private readonly EFRepository<Customer> _customerRepository;
        private readonly EFRepository<PromoCode> _promoCodeRepository;
        private readonly EFRepository<Preference> _preferenceRepository;

        public PromocodesController(EFRepository<Employee> employeeRepository,
            IMapper mapper, 
            EFRepository<Customer> customerRepository,
            EFRepository<PromoCode> promoCodeRepository,
            EFRepository<Preference> preferenceRepository)
        {
            _employeeRepository = employeeRepository;
            _customerRepository = customerRepository;
            _promoCodeRepository = promoCodeRepository;
            _preferenceRepository = preferenceRepository;
            _mapper = mapper;
        }
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            //TODO: Получить все промокоды 
            var promoCodes = await Task.Run(async () => await _promoCodeRepository.GetAllAsync());

            var response = _mapper.Map<List<PromoCodeShortResponse>>(promoCodes);
            return await Task.FromResult(response);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<string> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            //TODO: Создать промокод и выдать его клиентам с указанным предпочтением
            var promoCod = _mapper.Map<PromoCode>(request);
            promoCod.BeginDate = DateTime.Now.Date;
            promoCod.EndDate = promoCod.BeginDate.AddMonths(6);

            string[] fullName = request.PartnerName.Split(' ');
            var partnerManager = await Task.Run(async () => await _employeeRepository.GetByNameAsync(fullName));
            if (partnerManager == null)
                return await Task.FromResult("Указано некорректное имя менеджера.");

            promoCod.PartnerManagerId = partnerManager.Id;

            var preference = await Task.Run(async () => await _preferenceRepository.GetPreferenceByNameAsync(request.Preference));

            promoCod.PreferenceID = preference.Id;
            promoCod.CustomerId = preference.CustomerId;

            await Task.Run(async () => await _promoCodeRepository.CreateEntity(promoCod));
            return await Task.FromResult("Промокод добавлен");
        }
    }
}