﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CreateOrEditRole
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
