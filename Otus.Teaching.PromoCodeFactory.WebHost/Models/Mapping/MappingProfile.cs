﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Employee, EmployeeShortResponse>();
            CreateMap<CreateOrEditEmployee, Employee>();
            CreateMap<Employee, EmployeeResponse>();
            CreateMap<CreateOrEditCustomerRequest, Customer>();
            CreateMap<Customer, CustomerShortResponse>();
            CreateMap<Customer, CustomerResponse>();
            CreateMap<Role, RoleItemResponse>();
            CreateMap<CreateOrEditRole, Role>();
            CreateMap<Preference, PreferenceResponse>();
            CreateMap<PreferenceResponse, Preference>();
            CreateMap<GivePromoCodeRequest, PromoCode>().ForMember("Code", x=>x.MapFrom(x => x.PromoCode))
                                                        .ForMember(x => x.Preference, (options) => options.Ignore());
            CreateMap<PromoCode, PromoCodeShortResponse>();
            CreateMap<PromoCodeShortResponse, PromoCode>();
        }
    }
}
